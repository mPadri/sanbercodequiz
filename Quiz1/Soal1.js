import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

const FComponent = () => {
  const [name, setName] = useState('Jhon Doe');

  useEffect(() => {
    setTimeout(() => {
      setName('Asep');
    }, 3000);
  }, []);
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
  },
  text: {
    fontWeight: 'bold',
    fontSize: 32,
  },
});

export default FComponent;
