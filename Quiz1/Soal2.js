import React, {useContext} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import {RootContext} from './Context';

const Soal2 = () => {
  const state = useContext(RootContext);
  //   console.log(state.name);
  return (
    <View style={styles.container}>
      <FlatList
        data={state.name}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <View>
            <Text>Nama: {item.name}</Text>
            <Text>Position: {item.position}</Text>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
});

export default Soal2;
