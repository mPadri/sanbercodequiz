import React from 'react';
import {StyleSheet} from 'react-native';
import Soal1 from './Quiz1/Soal1';
import ContextAPI from './Quiz1/Context';

const App = () => {
  // return <Soal1 />;
  return <ContextAPI />;
};

const styles = StyleSheet.create({});

export default App;
